<?php

/**
 * @file
 * Authorize.Net Webform Fields File
 *
 * Prepare and store the available linking fields for Authorize.Net, as well
 * as custom fields that are defined by the user.
 */

/**
 * Provide the required fields for Authorize.Net processing
 *
 * @return array
 *  Provide an array containing the fields that are required by Authorize.Net
 */
function authorizenetwebform_available_fields() {
  return array(
    // Special Field to block Processing.
    0 => '--Do Not Map to an Authorize.Net field--',

    // Transaction Info.
    'x_trans_id' => 'Transaction ID (Required)',
    'x_trans_type' => 'Transaction type (Required)',
    'x_card_num' => 'Card Number (Required)',
    'x_card_code' => 'Card Security Code (Required)',
    //'x_exp_date' => 'Expiration Date (Date component)',
    'x_exp_month' => 'Expiration Month (2 Digits)',
    'x_exp_year' => 'Expiration Year (2 Digits)',
    'x_amount' => 'Amount (Required)',

    // Subscription info.
    'anwf_quantity' => 'Quantity - multiplies Amount by this number if set.',
    'x_subscription' => 'Subscription Name',

    // Cardholder Info.
    'x_first_name' => 'First Name (Required)',
    'x_last_name' => 'Last Name (Required)',
    'x_company' => 'Company',
    'x_address' => 'Address',
    'x_city' => 'City',
    'x_state' => 'State',
    'x_zip' => 'Zip',
    'x_email' => 'Email',
  );
}

/**
 * Provide any custom fields that users have defined in the admin section.
 *
 * @return $return_fields
 *  Provide an array containing any fields that were created by the user.
 */
function authorizenetwebform_load_custom_fields($loadtype) {
  $custom_fields = variable_get('authorizenetwebform_custom_fields', array());
  $return_fields = array();
  if (is_array($custom_fields)) {
    if ($loadtype == "webform") {
      foreach ($custom_fields as $cfkey => $cfvalue) {
        $cfkey = strtolower($cfkey);
        $return_fields[$cfkey] = $cfvalue;
      }
    }
    else {
      $return_fields = $custom_fields;
    }
    return $return_fields ;
  }
  else {
    return array();
  }
}
