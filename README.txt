Authorize.net Webform
=====================

This module extends the webform module to allow submitting a webform payment
through Authorize.net. The module does require PHP to be built with CURL
enabled in order to post the data to Authorize.net.

If you are having issues with blank screens or errors on form submissions,
as a first troubleshooting step, make sure you have followed all of the
instructions in INSTALL.txt.


Security
--------

This module will not work if your site does not have an SSL certificate.

Any time you are collecting credit card data or other sensitive information,
you should always transfer the sensitive data securely. A HTTPs connection is
the first step.


Installation
------------

- Install this module using the official Drupal instructions at
  https://www.drupal.org/docs/7/extend/installing-modules.

- Visit the configuration page under Administration -> Configuration -> Content
  -> Authorize.net Webform (admin/config/content/authorizenetwebform) and enter
  the required information.

- See INSTALL.txt for more information.


Issues
------

Bugs and Feature requests should be reported in the Issue Queue:
https://www.drupal.org/project/issues/authorizenetwebform


Current Maintainers
-------------------

- [kvnm](https://www.drupal.org/u/kvnm)
- [amariotti](https://www.drupal.org/u/amariotti)
- [jenlampton](https://www.drupal.org/u/jenlampton)
- [obsidiandesign](https://www.drupal.org/u/obsidiandesign)


Credits
-------

- Initially sponsored by by [Davis Applied Technology College](http://www.datc.edu).
- Initially programmed by [Obsidian Design LLP](http://www.obsidiandesign.com).
- Accept.js integration added by [Flatt & Sons](https://www.flattnet.com).
- Accept.js integration added by [Jeneration Web Development](https://www.jeneration.com).


License
-------

This project is GPL v2 software. See the LICENSE.txt file in this directory for
complete text.
