/**
 * @file
 * Authorizenetwebform JavaScript integration.
 */

(function($){ // Create local scope.
  Drupal.behaviors.authorizenetwebform = {
    attach: function (context) {

      // Find the weborm that submits to authnet.
      $(context).find("[data-authnet-form]").once(function () {

        // Get the whole form.
        var $webform = $(this);
        // Find the submit button.
        var $button = $(this).find("[data-authnet-submit]");
        // Get the field mapping.
        var mapping = Drupal.settings.authorizeNetWebform.mapping;

        // Submit to Authorize.net on click.
        $button.click(sendPaymentDataToAnet);

        /**
         * Sends the payment information to AuthNet.
         */
        function sendPaymentDataToAnet(e) {
          var secureData = {}, authData = {}, cardData = {};

          for (var key in Drupal.settings.authorizeNetWebform.mapping) {
            if (Drupal.settings.authorizeNetWebform.mapping.hasOwnProperty(key)) {
              var attribute = Drupal.settings.authorizeNetWebform.mapping[key];
              if ($webform.find("[" + attribute + "]")) {
                cardData[key] = $webform.find("[" + attribute + "]").val();
              }
            }
          }

          secureData.cardData  = cardData;

          authData.clientKey  = Drupal.settings.authorizeNetWebform.clientKey;
          authData.apiLoginID = Drupal.settings.authorizeNetWebform.apiLoginID;

          secureData.authData = authData;

          // @todo remove this stupid delay when AuthNet gets their act together.
          // https://community.developer.authorize.net/t5/Integration-and-Testing/E00114-Invalid-OTS-Token/td-p/58879/page/2
          setTimeout(function() {
            Accept.dispatchData(secureData, 'responseHandler');
          }, Drupal.settings.authorizeNetWebform.delay);

          // Prevent the webform from submitting now. @see last line.
          e.preventDefault();
        }

        /**
         * Handles the response from AuthNet.
         */
        responseHandler = function(response) {
          // Remove old errors.
          $webform.find(".messages").remove();

          // Handle errors if there was a problem.
          if (response.messages.resultCode === 'Error') {
            var codes = [];
            // Print the error messages.
            for (var i = 0; i < response.messages.message.length; i++) {
              console.log(response.messages.message[i].code + ': ' + response.messages.message[i].text);
              $webform.prepend('<div class="messages error">' + response.messages.message[i].code + ': ' + response.messages.message[i].text + '</div>');
              codes.push(response.messages.message[i].code);
            }

            // Mark the form fields related to each error code.
            if (codes.indexOf('E_WC_05') >= 0) {
              $webform.find('[' + mapping.cardNumber + ']').addClass('error');
            }
            else {
              $webform.find('[' + mapping.cardNumber + ']').removeClass('error');
            }
            if (codes.indexOf('E_WC_06') >= 0) {
              $webform.find('[' + mapping.month + ']').addClass('error');
            }
            else {
              $webform.find('[' + mapping.month + ']').removeClass('error');
            }
            if (codes.indexOf('E_WC_07') >= 0) {
              $webform.find('[' + mapping.year + ']').addClass('error');
            }
            else {
              $webform.find('[' + mapping.year + ']').removeClass('error');
            }
            if (codes.indexOf('E_WC_15') >= 0) {
              $webform.find('[' + mapping.cardCode + ']').addClass('error');
            }
            else {
              $webform.find('[' + mapping.cardCode + ']').removeClass('error');
            }

            // Scroll to errors for better UX.
            $([document.documentElement, document.body]).animate({
              scrollTop: $(".messages").offset().top
            }, 1000);

          }
          else {
            useOpaqueData(response.opaqueData);
          }
        }

        /**
         * Updtes the webform with the Payment Noce returned from AuthNet, and
         * effectively removes credit card details from the form.
         */
        useOpaqueData = function(responseData) {
          // For Debugging.
          //console.log(responseData.dataDescriptor);
          //console.log(responseData.dataValue);
          //alert(responseData.dataValue);

          // Insert the Payment Nonce into the hidden Transaction ID field.
          $(context).find("[data-authnet-field--transaction_id]").val(responseData.dataValue);

          // Remove the name attributes to prevent values from touching the server.
          var sensitive = ['cardNumber', 'cardCode', 'month', 'year'];
          for (var key in Drupal.settings.authorizeNetWebform.mapping) {
            if (jQuery.inArray(key, sensitive) !== -1) {
              var attribute = Drupal.settings.authorizeNetWebform.mapping[key];
              if ($webform.find("[" + attribute + "]")) {
                $webform.find("[" + attribute + "]").removeAttr('name');
              }
            }
          }

          // Submit the webform when done.
          $(context).find("[data-authnet-form]").submit();
        }

      });
    }
  };

})(jQuery);
