<?php
/**
 * @file
 * Admin interface for Authorize.net webform module.
 */

/**
 * Page callback for admin settings page.
 */
function authorizenetwebform_admin_settings() {
  $form = array();

  $mode = variable_get('authorizenetwebform_mode', 'main');
  $form['authorizenetwebform_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Transaction mode'),
    '#description' => t('Unless you are using a sandbox account, this should be set to Live.'),
    '#options' => array(
      'main' => t('Live account'),
      'test' => t('Test (or Sandbox) account'),
    ),
    '#default_value' => $mode,
  );
  // Add description text for URL choices.
  $form['authorizenetwebform_mode']['test']['#description'] = t('Posts to https://test.authorize.net/gateway/transact.dll');
  $form['authorizenetwebform_mode']['main']['#description'] = t('Posts to https://secure.authorize.net/gateway/transact.dll');

  $form['authorizenetwebform_recurring'] = array(
    '#type' => 'markup',
    '#markup' => '<h3>' . t('Recurring transactions') . '</h3><p>' . t('Using the recurring payments option will immedialtey run a one-time transaction for the amount. Then a recurring payment transaction will be set up, and this will begin on the 2nd month.') . '</p>',
  );

  $form['login'] = array(
    '#type' => 'fieldset',
    '#title' => t('Authorize.net Login'),
  );
  $form['login']['authorizenetwebform_login'] = array(
    '#type' => 'textfield',
    '#title' => t('API Login ID'),
    '#description' => t('Enter your Authorize.Net API Login ID - NOT your username.'),
    '#default_value' => variable_get('authorizenetwebform_login', ''),
  );
  $form['login']['authorizenetwebform_transaction'] = array(
    '#type' => 'textfield',
    '#title' => t('Transaction key'),
    '#description' => t('Enter your Authorize.Net Transaction Key.  It will not be displayed here once it is entered, for security purposes.  If you need to change it, please re-enter it.'),
    '#default_value' => variable_get('authorizenetwebform_transaction', ''),
  );
  $form['login']['authorizenetwebform_client_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Public Client Key'),
    '#description' => t('To generate the Client Key, log in to the Merchant Interface as an Administrator and navigate to Account > Settings > Security Settings > General Security Settings > Manage Public Client Key. If the Public Client Key does not yet exist, answer your security question to generate the key.'),
    '#default_value' => variable_get('authorizenetwebform_client_key', ''),
  );
  if ($mode == 'test') {
    $form['login']['authorizenetwebform_client_key']['#description'] .= t(' <strong>A SANDBOX Key will expire 24 hours after creation.</strong>');
  }

  $form['field_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Authorize.Net custom field mappings'),
    '#description' => t('Define custom webform field mappings for sending additional data to Authorize.Net'),
  );
  $form['field_settings']['authorizenetwebform_custom_fields'] = array(
    '#type' => 'textarea',
    '#title' => t("Custom Fields"),
    '#default_value' => authorizenet_create_custom_field_spec(variable_get('authorizenetwebform_custom_fields', NULL)),
    '#description' => t('A list of additional fields configured for transactions to Authorize.Net. One field per line, with field name and description seperated by pipes. i.e. safe_key|Some readable option.  See the Implementation Guide at http://developer.authorize.net/guides/AIM/ for more details.'),
    '#rows' => 5,
  );

  $delay = variable_get('authorizenetwebform_delay', 10);
  $form['authorizenetwebform_delay'] = array(
    '#type' => 'textfield',
    '#title' => t('Delay'),
    '#description' => t('If you are seeing an intermittent E00114 Invalid OTS Token error, you may need to implement a slight delay.'),
    '#default_value' => $delay,
    '#field_suffix' => format_plural($delay, 'Second', 'Seconds'),
    '#size' => 10,
  );

  $form['authorizenetwebform_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Debug mode'),
    '#description' => t('When enabled, the complete response from Auth.net will be stored.<br/>If the <em>Devel</em> module is enabled, the complete response from Auth.net will also be printed to the screen.'),
    '#default_value' => variable_get('authorizenetwebform_debug', FALSE),
  );

  $form['#validate'][] = 'authorizenetwebform_parse_custom_field_spec';

  return system_settings_form($form);
}

/**
 * Validation handler for authorizenetwebform_admin_settings().
 */
function authorizenetwebform_parse_custom_field_spec($form, &$form_state) {
  $custom_fields = array();
  $custom_field_spec = $form_state['values']['authorizenetwebform_custom_fields'];
  if ( !empty($custom_field_spec) ) {
    $custom_field_tmp = explode("\n", $custom_field_spec);
    foreach ( $custom_field_tmp as $field ) {
      if ( empty($field) ) break ;
      $new_field = explode("|", $field);
      $custom_fields[$new_field[0]] = $new_field[1];
    }
  }
  form_set_value($form['field_settings']['authorizenetwebform_custom_fields'], $custom_fields, $form_state);
}

/**
 * Helper function: Generates default values.
 *
 * @see authorizenetwebform_admin_settings().
 */
function authorizenet_create_custom_field_spec($custom_fields) {
  if ( empty($custom_fields) ) {
    return '';
  }

  $spec = '';
  foreach ( array_keys($custom_fields) as $field ) {
    $field_string = $field . "|" . $custom_fields[$field] . "\n";
    $spec .= $field_string;
  }
  return $spec;
}
